package events

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

type ReadyHandler struct{}

func NewReadyHandler() *ReadyHandler {
	return &ReadyHandler{}
}

func (h *ReadyHandler) Handler(s *discordgo.Session, r *discordgo.Ready) {
	log.Println("Bot session is ready")
	log.Printf("Bot is %v\n\n", r.User.String())
}
