package events

import (
	"fmt"
	"log"

	"github.com/bwmarrin/discordgo"
)

type MessageHandler struct{}

func NewMessageHandler() *MessageHandler {
	return &MessageHandler{}
}

func (h *MessageHandler) Log(s *discordgo.Session, m *discordgo.MessageCreate) {
	channel, err := s.Channel(m.ChannelID)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%v%v at %v\n", channel.Name, m.ChannelID, m.Timestamp)
	fmt.Printf("%v: ", m.Author)
	if m.TTS {
		fmt.Print("/tts ")
	}
	fmt.Printf("%v\n\n", m.Content)
}
