package commands

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Lisp struct{}

var popper int

func (cmd *Lisp) Invoke() string {
	return "eval"
}

func (cmd *Lisp) Exec(ctx *Context) (err error) {
	msg, err := parse(tokenize(strings.Join(ctx.Args[:], " ")))
	if err != nil {
		return
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, fmt.Sprint(msg))
	popper = 0
	return
}

func tokenize(chars string) []string {
	return strings.Split(strings.Replace(strings.Replace(chars, "(", "( ", -1), ")", " )", -1), " ")
}

func parse(tokens []string) (interface{}, error) {
	if len(tokens) == 0 {
		return nil, errors.New("unexpected EOF")
	}
	token := tokens[popper]
	popper++
	if token == "(" {
		list := make([]interface{}, 0)
		for token != ")" {
			newlist, err := parse(tokens)
			if err != nil {
				break
			}
			list = append(list, newlist)
		}
		return list, nil
	}
	if token == ")" {
		return nil, errors.New("unexpected )")
	}
	return atom(token)
}

func atom(token string) (interface{}, error) {
	if atom, err := strconv.Atoi(token); err == nil {
		return atom, err
	}
	if atom, err := strconv.ParseFloat(token, 64); err == nil {
		return atom, err
	}
	return token, nil
}
