package commands

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

type CommandHandler struct {
	prefix       string
	cmdInstances []Command
	cmdMap       map[string]Command
	middlewares  []Middleware
	OnError      func(ctx *Context, err error)
}

func NewCommandHandler(prefix string) *CommandHandler {
	return &CommandHandler{
		prefix:       prefix,
		cmdInstances: make([]Command, 0),
		cmdMap:       make(map[string]Command),
		middlewares:  make([]Middleware, 0),
		OnError:      func(*Context, error) {},
	}
}

func (h *CommandHandler) RegisterCommand(cmd Command) {
	h.cmdInstances = append(h.cmdInstances, cmd)
	h.cmdMap[cmd.Invoke()] = cmd
}

func (h *CommandHandler) RegisterMiddleware(mw Middleware) {
	h.middlewares = append(h.middlewares, mw)
}

func (h *CommandHandler) HandleMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.Bot || !strings.HasPrefix(m.Content, h.prefix) {
		return
	}
	split := strings.Split(m.Content[len(h.prefix):], " ")
	if len(split) < 1 {
		return
	}
	invoke := strings.ToLower(split[0])
	args := split[1:]
	cmd, ok := h.cmdMap[invoke]
	if !ok {
		return
	}
	ctx := &Context{
		Session: s,
		Args:    args,
		Handler: h,
		Message: m.Message,
	}
	for _, v := range h.middlewares {
		next, err := v.Exec(ctx, cmd)
		if err != nil {
			h.OnError(ctx, err)
			return
		}
		if !next {
			return
		}
	}
	if err := cmd.Exec(ctx); err != nil {
		h.OnError(ctx, err)
	}
}
