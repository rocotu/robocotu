package commands

import (
	"bufio"
	"math/rand"
	"os"
	"time"
)

type Fortune struct{}

func (cmd *Fortune) Invoke() string {
	return "fortune"
}

func (cmd *Fortune) Exec(ctx *Context) (err error) {
	rand.Seed(time.Now().UnixNano())
	file, err := os.Open("./config/fortunes")
	if err != nil {
		return
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var fortunes []string
	for scanner.Scan() {
		fortunes = append(fortunes, scanner.Text())
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, fortunes[rand.Intn(len(fortunes)-1)])
	return
}
