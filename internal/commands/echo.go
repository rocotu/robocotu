package commands

import "strings"

type Echo struct{}

func (cmd *Echo) Invoke() string {
	return "echo"
}

func (cmd *Echo) Exec(ctx *Context) (err error) {
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, strings.Join(ctx.Args[:], " "))
	return
}
