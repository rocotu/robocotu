package commands

type Command interface {
	Invoke() string
	Exec(ctx *Context) error
}
