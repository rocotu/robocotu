package commands

import (
	"math/rand"
	"strconv"
	"time"
)

type Minesweeper struct{}

var n int

func (cmd *Minesweeper) Invoke() string {
	return "minesweeper"
}

func (cmd *Minesweeper) Exec(ctx *Context) (err error) {
	rand.Seed(time.Now().UnixNano())
	var msg string
	emojis := map[int]string{
		-1: ":boom:",
		0:  ":white_large_square:",
		1:  ":one:",
		2:  ":two:",
		3:  ":three:",
		4:  ":four:",
		5:  ":five:",
		6:  ":six:",
		7:  ":seven:",
		8:  ":eight:",
	}
	if len(ctx.Args) < 1 {
		n = 10
	} else {
		n, err = strconv.Atoi(ctx.Args[0])
		if err != nil {
			return
		}
	}
	board := make([][]int, n)
	for i := range board {
		board[i] = make([]int, n)
	}
	for i := 0; i < n; {
		x := rand.Intn(len(board))
		y := rand.Intn(len(board))
		if board[x][y] == 0 {
			board[x][y] = -1
			i++
		}
	}
	for i := range board {
		for j := range board[i] {
			msg += "||" + emojis[surrounded(board, i, j)] + "||"
		}
		msg += "\n"
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, msg)
	return
}

func surrounded(board [][]int, x, y int) int {
	if board[x][y] < 0 {
		return -1
	}
	var around int
	if x-1 >= 0 && board[x-1][y] < 0 {
		around++
	}
	if x-1 >= 0 && y+1 < n && board[x-1][y+1] < 0 {
		around++
	}
	if y+1 < n && board[x][y+1] < 0 {
		around++
	}
	if x+1 < n && y+1 < n && board[x+1][y+1] < 0 {
		around++
	}
	if x+1 < n && board[x+1][y] < 0 {
		around++
	}
	if x+1 < n && y-1 >= 0 && board[x+1][y-1] < 0 {
		around++
	}
	if y-1 >= 0 && board[x][y-1] < 0 {
		around++
	}
	if x-1 >= 0 && y-1 >= 0 && board[x-1][y-1] < 0 {
		around++
	}
	return around
}
