package commands

import (
	"math/rand"
	"time"
)

type Hellokitty struct{}

func (cmd *Hellokitty) Invoke() string {
	return "hello"
}

func (cmd *Hellokitty) Exec(ctx *Context) (err error) {
	rand.Seed(time.Now().UnixNano())
	var reply string
	if len(ctx.Args) < 1 || ctx.Args[0] != "kitty" {
		if rand.Intn(2) < 1 {
			reply = ":?"
		} else {
			reply = "Hello"
		}
		_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, reply)
		return
	}
	switch ctx.Message.Author.ID {
	case "448310061379485696":
		reply = "child"
	default:
		reply = "uwu"
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, reply)
	return
}
