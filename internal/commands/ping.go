package commands

type Ping struct{}

func (cmd *Ping) Invoke() string {
	return "ping"
}

func (cmd *Ping) Exec(ctx *Context) (err error) {
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, "pong")
	return
}
