package commands

import "strings"

type Say struct{}

func (cmd *Say) Invoke() string {
	return "say"
}

func (cmd *Say) Exec(ctx *Context) (err error) {
	if err = ctx.Session.ChannelMessageDelete(ctx.Message.ChannelID, ctx.Message.Reference().MessageID); err != nil {
		return
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, strings.Join(ctx.Args[:], " "))
	return
}
