package commands

import (
	"bufio"
	"math/rand"
	"os"
	"time"
)

type Wordle struct{}

func (cmd *Wordle) Invoke() string {
	return "wordle"
}

func (cmd *Wordle) Exec(ctx *Context) (err error) {
	_, msg, err := randWord()
	if err != nil {
		return
	}
	_, err = ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, msg)
	return
}

func randWord() (words []string, word string, err error) {
	rand.Seed(time.Now().UnixNano())
	/*
		resp, err := http.Get("https://raw.githubusercontent.com/tabatkins/wordle-list-main/words")
		if err != nil {
			return
		}
		defer resp.Body.Close()
		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return
		}
	*/
	file, err := os.Open("./config/words")
	if err != nil {
		return
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		words = append(words, scanner.Text())
	}
	word = words[rand.Intn(len(words)-1)]
	return
}
