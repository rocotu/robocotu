package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	Token string `json:"token"`
}

func ParseConfig(fileName string) (c *Config, err error) {
	f, err := os.Open(fileName)
	if err != nil {
		return
	}
	c = new(Config)
	err = json.NewDecoder(f).Decode(c)
	return
}
