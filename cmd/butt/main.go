package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/rocotu/robocotu/internal/commands"
	"gitlab.com/rocotu/robocotu/internal/config"
	"gitlab.com/rocotu/robocotu/internal/events"
)

const file string = "./config/token.json"

var BotID string

func main() {
	log.Println("Parsing config...")
	cfg, err := config.ParseConfig(file)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Success!")
	log.Println("Authenticating...")
	dg, err := discordgo.New("Bot " + cfg.Token)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Success!")
	log.Println("Starting bot...")
	dg.Identify.Intents = (discordgo.IntentsGuildMessages | discordgo.IntentsGuildMessageReactions | discordgo.IntentsGuildMessageTyping | discordgo.IntentsDirectMessages | discordgo.IntentsDirectMessageReactions | discordgo.IntentsDirectMessageTyping | discordgo.IntentMessageContent)
	registerEvents(dg)
	registerCommands(dg, ".r ")
	if err = dg.Open(); err != nil {
		log.Fatalln(err)
	}
	log.Println("Success!")
	defer dg.Close()
	log.Println("It's alive!")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func registerEvents(s *discordgo.Session) {
	s.AddHandler(events.NewReadyHandler().Handler)
	s.AddHandler(events.NewMessageHandler().Log)
}

func registerCommands(s *discordgo.Session, prefix string) {
	cmdHandler := commands.NewCommandHandler(prefix)
	s.AddHandler(cmdHandler.HandleMessage)
	cmdHandler.RegisterCommand(&commands.Ping{})
	cmdHandler.RegisterCommand(&commands.Echo{})
	cmdHandler.RegisterCommand(&commands.Say{})
	cmdHandler.RegisterCommand(&commands.Fortune{})
	cmdHandler.RegisterCommand(&commands.Minesweeper{})
	cmdHandler.RegisterCommand(&commands.Lisp{})
	cmdHandler.RegisterCommand(&commands.Wordle{})
	cmdHandler.RegisterCommand(&commands.Hellokitty{})
	cmdHandler.OnError = func(ctx *commands.Context, err error) {
		ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, fmt.Sprintf("`%v`", err.Error()))
	}
}
